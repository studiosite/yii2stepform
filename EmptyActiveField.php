<?php

namespace studiosite\yii2stepform;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Заглушка инпута для полей не требующих отрисовки
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class EmptyActiveField
{
	/**
    * @inheritdoc
    */
	public function __call($name, $params = [])
	{
		return '';
	}
}