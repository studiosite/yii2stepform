<?php

namespace studiosite\yii2stepform;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Базовая пошаговая db-модель с трейдом 
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
	/**
	* Трейд с фсем функционалом для модели
	*/
	use StepTrait;

	/**
    * @var string[] Список сценариев, нечто вроде карты по которой пользователь будет идти по мере заполнения формы
    */
    public $scenarioSteps = [];

    /**
    * При инициализации необходимо указате текущий сценарий и загрузить данные по предыдущим шагам
    */
    public function init()
    {
       parent::init();

       $this->setCurrentScenario();
    }
}