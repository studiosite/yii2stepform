<?php

namespace studiosite\yii2stepform;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Обячная bootstrap форма c использование своих полей
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class ActiveForm extends \yii\bootstrap\ActiveForm
{
    /**
    * Переопределенный высов сохдания поля
    *
    * @return \yii\base\ActiveField|EmptyActiveField
    */
    public function field($model, $attribute, $options = [])
    {
    	$scenarios = $model->scenarios();

		if (isset($scenarios[$model->scenario])) {
			if (empty($scenarios[$model->scenario]))
				return parent::field($model, $attribute, $options);

			if (in_array($attribute, $scenarios[$model->scenario])) 
				return parent::field($model, $attribute, $options);
		}

		return new EmptyActiveField();
    }
}