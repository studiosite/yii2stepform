yii2stepform
=================

Пошаговая форма. Одна форма с несколькими шагами. Удабна для форм с подтверждением

## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

```
"studiosite\yii2stepform": "dev-master"
```

в секции ```require``` `composer.json` файла.

В секции ```repositories``` добавить

```
{
    "type": "vcs",
    "url": "https://bitbucket.org/studiosite/yii2stepform"
}
```

## Использование

Необходимую форму требуется унаследовать от  одного из классов ```studiosite\yii2stepform\Model``` ```studiosite\yii2stepform\ActiveRecord```

и сконфигурировать карту сценариев ```scenarioSteps```
```php
class MyModel extends \studiosite\yii2stepform\Model
{
  	/**
    * @var string[] Список сценариев, нечто вроде карты по которой пользователь будет идти по мере заполнения формы
    */
    public $scenarioSteps = [
  		'login'
      	'loginConfirm'
	];
  	
  	// Свои поля
    public $phone;
    public $name;
  	public $code;
  	
  	// Свои правила с сценариями (можно указать через метод scenario)
  	public function rules()
    {
  		return [
  			[['phone', 'name'], 'required', 'on' => 'login'],
            [['oneTimePassword'], 'required', 'on' => 'loginConfirm'], 
		];
    }
}
```



Содержимое переменной ```scenarioSteps``` - список имеющихся сценариев в модели в порядке прохождения шагов.

В контроллере используется обычная конструкция

```php
// Экшен
public function actionAuth()
{
    // В сценарии модели нужно указать любой из шагов (в зависимости от того, с кого момента у вас должна начинаться форма) либо оставить пустым (\yii\base\Model::SCENARIO_DEFAULT) - в этом случае будет использоваться первый сценарий из списка scenarioSteps
  	$model = new MyModel(['scenario' => 'login']);
  
	if ($model->load(Yii::$app->request->post())) {
    	if (\yii::$app->request->isAjax) {
      		\yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

      		return ActiveForm::validate($model);
    } else {
    	if ($model->validate()) {
            if (!$model->nextScenario()) {
               	$model->clearSteps();
              
              // В этом месте, все шаги выполнены (на каждом шаге форма была валидна правилам MyModel::rules()), и можно сохранять модель или делать необходимые действия
                // Внимание. Сценарий у модели в этом месте == последнему сценарию из списка scenarioSteps. Это может повлиять на сохранение модели. Поэтому следует сделать общий сценарий модели и указывать его в этом месте перед сохранением # $model->scenario = 'commonLogin'; $model->save();
                if (!$model->login()) {
  					throw new \Exception('Access denied');
				}
                
              	return $this->goBack();
         	}
        }
    }
}
```

В отображении, если форма тривиальная, можно воспользоваться виджетом ```studiosite\yii2stepform\ActiveForm```. 

```php
use studiosite\yii2stepform\ActiveForm;

?>

<?php $form = ActiveForm::begin([
   		'enableAjaxValidation' => true,
]); ?>

<?= $form->field($model, 'phone')->input('phone') ?>
<?= $form->field($model, 'name')->input('text') ?>
<?= $form->field($model, 'code')->input('text') ?>
  
<?php $form->end() ?>
  
```

В этом примере, в зависимосте от текущего шага сценариев будут отрисованы либо полностью скрыты поля. # При сценарии ```login``` будет скрыто поле ```code```, а для сценария ```loginConfirm``` не будут отрисованы поля ```phone``` и ```name```

Если не использовать виджет ```studiosite\yii2stepform\ActiveForm``` можно в зависимости от сценария отрисовывать необходимые инпуты, либо содержание уже заполненных - но все руками.

#### Использование трейта

Если нет возможности использования одной из подготовленных моделей ```studiosite\yii2stepform\Model``` ```studiosite\yii2stepform\ActiveRecord```, то можно использовать трейт ```studiosite\yii2stepform\StepTrait```, но в этом случае должена обязательно быть указана переменная  ```scenarioSteps``` и при инициализации модели вызван метод ```setCurrentScenario()```