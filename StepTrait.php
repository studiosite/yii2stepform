<?php

namespace studiosite\yii2stepform;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\Json;

/**
 * Тред для пошаговых моделей
 *
 * @todo Сделать пробег по scenarioSteps foreach
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
trait StepTrait
{
    /**
    * @var integer the number of seconds after which data will be seen as 'garbage' and cleaned up
    */
    public $sessionTimeOut;

    private $_session;

    private function _getSession()
    {
        if (empty($this->_session)) {
            $this->_session = Yii::$app->getSession();
            $this->_session->open();

            if (!empty($this->sessionTimeOut)) {
                $this->_session->setTimeout($this->sessionTimeOut);
            }
        }

        return $this->_session;
    }

     /**
     * Установка сценария
     *
     * @throws InvalidParamException
     */
     public function setCurrentScenario()
     {
     	if (empty($this->scenarioSteps))
     		throw new InvalidParamException("Steps scenario not configured. See \$this->scenarioSteps");


        if (empty($this->scenario) || $this->scenario==\yii\base\Model::SCENARIO_DEFAULT)
            $this->scenario = $this->scenarioSteps[0];

        if (array_search($this->scenario, $this->scenarioSteps)===false)
            throw new InvalidParamException("Invalid configured steps scenario. Can not find scenario \"".$this->scenario."\". See \$this->scenarioSteps in model");

        if (($lastScenarioStepIndex = array_search($this->_getSession()->get($this->unicalSessionKey), $this->scenarioSteps))===false)
            return;

        $loadSuccess = true;
        for ($i=0; $i < $lastScenarioStepIndex; $i++) { 
            $historyScenario = $this->scenarioSteps[$i];
            $this->scenario = $historyScenario;

            $this->load([self::formName() => Json::decode($this->_getSession()->get($this->unicalSessionKey."_".$historyScenario))]);

            if (!$this->validate()) {
                $loadSuccess = false;
            }
        }

        if ($loadSuccess) {
            $this->scenario = $this->scenarioSteps[$lastScenarioStepIndex];
        } else {
            $this->clearSteps();
        }
     }

     /**
     * Полная очистка всех сохраненных шагов
     * Необходимо вызывать после ($this->nextStep()==false)
     */
     public function clearSteps()
     {
        $uKey = $this->unicalSessionKey;
        $this->_getSession()->remove($uKey);
        foreach ($this->scenarioSteps as $key) {
            $this->_getSession()->remove($uKey."_".$key);
        }   
     }

     /**
     * Слудующий шаг
     *
     * @throws InvalidParamException
     * @param boolean В случае неуспеха вернет false, это будет означать, что сцерии закончились и можно продолжать выполняемые действия
     */
     public function nextScenario()
     {
     	if (empty($this->scenarioSteps))
     		throw new InvalidParamException("Steps scenario not configured. See \$this->scenarioSteps");

        $currentIndex = array_search($this->scenario, $this->scenarioSteps);

        if ($currentIndex!==false) {
            if (count($this->scenarioSteps)>($currentIndex+1)) {
                $newScenario = $this->scenarioSteps[$currentIndex+1];
                $this->_getSession()->set($this->unicalSessionKey, $newScenario);
                $this->_getSession()->set($this->unicalSessionKey."_".$this->scenario, Json::encode($this->attributes));
                $this->scenario = $newScenario;

                return true;
            }

            return false;
        }

        return false;
     }

     /**
     * Предыдущий шаг
     * 
     * @throws InvalidParamException
     * @param boolean Вернет false в случае начала шагов
     */
     public function prevScenario()
     {
     	if (empty($this->scenarioSteps))
     		throw new InvalidParamException("Steps scenario not configured. See \$this->scenarioSteps");

        $currentIndex = array_search($this->scenario, $this->scenarioSteps);

        if ($currentIndex!==false) {
            if ($currentIndex>0) {
                $newScenario = $this->scenarioSteps[$currentIndex-1];
                $this->_getSession()->set($this->unicalSessionKey, $newScenario);
                $this->scenario = $newScenario;

                return true;
            }
        }

        $this->clearSteps();

        return false;
     } 

     /**
     * Получить уникальный ключ для сессии
     *
     * @return string
     */
     public function getUnicalSessionKey()
     {
        return self::className()."_".date("Y-m-d");
     }
}